# TallyFiles

## Introduction

TallyFiles was created as the answer to a Coding Snack on DonationCoder

    Suppose we have a folder with 100 files.
    Objective is to create a text file with a list of dates and tally of those dates based on the 100 files.
    Example:
    2018-09-22 7
    2018-09-19 8
    2018-09-14 11
    ...
    2018-09-3 1

    Space separator between date and tally number.
    No spaces within date.
    Result can be sorted, or not.  Preference is to sort by date.
    File name output should be same as folder name.  Plus .txt.
    No need for GUI.  Command-line preferred, for creation of batch files (say, acting on lots of different folders).

## Commandline Options


    [Option('d', "directory", Required = true, HelpText = "Directory to Scan")]
    [Option('f', "format", Required = false, HelpText = "Format of Date (Default yyyy-MM-dd)")]
    [Option('o', "output", Required = false, HelpText = "output filename (default name of directory + .txt)")]
    [Option('c', "compare", Required = false, HelpText = "compare to [C]reation Date or Last [A]ccess Date or Last [W]rite Date (default C)")]
    [Option('s', "savedir", Required=false, HelpText = "Save directory.  By default the file stored the in directory where tallyfiles is run")]



