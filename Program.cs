﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace TallyFiles
{
    public class Options
    {
        [Option('d', "directory", Required = true, HelpText = "Directory to Scan")]
        public string Directory { get; set; }
        [Option('f', "format", Required = false, HelpText = "Format of Date (Default yyyy-MM-dd)")]
        public string Format { get; set; }
        [Option('o', "output", Required = false, HelpText = "output filename (default name of directory + .txt)")]
        public string Output { get; set; }
        [Option('c', "compare", Required = false, HelpText = "compare to [C]reation Date or Last [A]ccess Date or Last [W]rite Date (default C)")]
        public string Compare { get; set; }
        [Option('s', "savedir", Required=false, HelpText = "Save directory.  By default the file stored the in directory where tallyfiles is run")]
        public string SaveDir { get; set; }
    }

    public enum CompareType
    {
        ctCreateDate = 0,
        ctAccessDate = 1,
        ctWriteDate = 2
    }

    class Program
    {
        static void Main(string[] args)
        {
            string saveDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string directory = null;
            string format = "yyyy-MM-dd";
            string output = null;
            CompareType compareType = CompareType.ctCreateDate;

            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    directory = o.Directory;
                    if (o.Format != null)
                    {
                        format = o.Format;
                    }

                    if (o.SaveDir != null && Directory.Exists(o.SaveDir))
                    {
                        saveDirectory = o.SaveDir;
                    }

                    if (o.Output != null)
                    {
                        output = Path.Combine(saveDirectory, o.Output);
                    }
                    else
                    {
                        output = string.Concat(Path.GetFileName(o.Directory), ".txt");
                        output = Path.Combine(saveDirectory, output);
                    }

                    if (o.Compare != null)
                    {
                        if (string.Compare(o.Compare, "W", true) == 0)
                        {
                            compareType = CompareType.ctWriteDate;
                        }
                        else if (string.Compare(o.Compare, "A", true) == 0)
                        {
                            compareType = CompareType.ctAccessDate;
                        }
                    }
                });

            if (Directory.Exists(directory))
            {
                DirectoryInfo info = new DirectoryInfo(directory);
                List<DateTime> fileDates;

                switch (compareType)
                {
                    case CompareType.ctAccessDate:
                        fileDates = info.GetFiles().OrderBy(p => p.LastAccessTime).Select(p => p.LastAccessTime.Date).ToList();
                        break;
                    case CompareType.ctWriteDate:
                        fileDates = info.GetFiles().OrderBy(p => p.LastWriteTime).Select(p => p.LastWriteTime.Date).ToList();
                        break;
                    default:
                        fileDates = info.GetFiles().OrderBy(p => p.CreationTime).Select(p => p.CreationTime.Date).ToList();
                        break;
                }

                var dateGroups = fileDates.GroupBy(i => i);
                using (var fileStream = new StreamWriter(output))
                {
                    foreach (var grp in dateGroups)
                    {
                        fileStream.WriteLine($"{grp.Key.ToString(format)} {grp.Count()}");
                    }
                }
            }
            else
            {
                Console.WriteLine($"Directory {directory} does not exist");
            }

        }
    }
}
